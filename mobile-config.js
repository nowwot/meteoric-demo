// This section sets up some basic app metadata,
// the entire section is optional.
App.info({
  id: 'com.hinaudigital.meteoricdemo',
  name: 'Nowwot',
  description: 'Choose life',
  author: 'Hinau Digital Limited',
  email: 'community@nowwot.co',
});
 
// Set up resources such as icons and launch screens.
App.icons({
  //'iphone': 'icons/icon-60.png',
  //'iphone_2x': 'icons/icon-60@2x.png',
});
 
App.launchScreens({
  //'iphone': 'splash/Default~iphone.png',
  //'iphone_2x': 'splash/Default@2x~iphone.png',
});
 
// Set PhoneGap/Cordova preferences
// App.setPreference('BackgroundColor', '0xff0000ff');
// App.setPreference('HideKeyboardFormAccessoryBar', true);
//  
// // Pass preferences for a particular PhoneGap/Cordova plugin
// App.configurePlugin('com.phonegap.plugins.facebookconnect', {
//   APP_ID: '821767551211913',
//   API_KEY: '3176f32e6fe507c319f9626418d2d3bc',
//   APP_NAME: 'Nowwot'
// });
